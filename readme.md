# Machine Learning tool with openCV
this project uses python and openCV to compleat a handwritten machine learing process.

We give the python program a picture with letters from A-F which the program uses to learn the letters and test its accuracy.

## To run program

to run the program you will need python and openCV which can be downloaded from the links below.

```
https://www.python.org/downloads/

https://pypi.org/project/opencv-python/
```

after downloading you can run the program using
```
python ML.py
```

## Contributor
Alexander Øvergård